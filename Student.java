public class Student {
    private String name;
    private int age;
    private int id;
    private static int idCounter = 1;


    public Student(String name, int age) {
        this.name = name;
        this.age = age;
        this.id = idCounter++;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
    public int getId() {
        return id;
    }


    public static void main(String[] args) {
        Student s1 = new Student("Alice", 20);
        Student s2 = new Student("Bob", 22);
        Student s3 = new Student("Charlie", 19);

        System.out.println("Student 1: " + s1.getName() + ", Age: " + s1.getAge() + ", ID: " + s1.getId());
        System.out.println("Student 2: " + s2.getName() + ", Age: " + s2.getAge() + ", ID: " + s2.getId());
        System.out.println("Student 3: " + s3.getName() + ", Age: " + s3.getAge() + ", ID: " + s3.getId());
    }
}

